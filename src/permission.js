import router from './router'
import { Message } from 'element-ui'

router.beforeEach((to, from, next) => {
  if (to.path.indexOf('userCenter') === 1 && !sessionStorage.getItem('token')) {
    Message.error('您还没有登陆，请先登陆！')
    next({ path: '/login' })
  } else {
    next()
  }
})