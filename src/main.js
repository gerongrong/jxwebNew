import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/global.scss'
import '@/styles/self.scss'

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import Element from 'element-ui'
import './permission'

Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.use(Element, { size: 'medium' })

/**
 * @description: 该方法是接口调用后的回应操作
 * @param {Number} code 接口code码
 * @param {String} sucStr 成功按钮text
 * @param {String} errStr 失败按钮text
 * @param {Function} nextSucAction 成功后回调
 * @param {Function} nextErrAction 失败后回调
 * @param {Array} sucArg 成功后回调函数参数
 * @param {Array} errArg 失败后回调函数参数
 */
Vue.prototype.apiResponse = function(code, { sucStr = '操作成功', errStr = '操作失败', nextSucAction, nextErrAction, sucArg = [], errArg = [] } = {}) {
  if (Number(code) === 200) {
    this.$alert(sucStr, '提示', {
      confirmButtonText: '确定',
      callback: action => {
        if (nextSucAction) nextSucAction.apply(this, sucArg);
      }
    })
  } else {
    this.$alert(errStr, '提示', {
      confirmButtonText: '确定',
      callback: action => {
        if (nextErrAction) nextErrAction.apply(this, errArg)
      }
    });
  }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})