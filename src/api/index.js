import axios from 'axios'
import qs from 'qs'
import store from '@/store'
import router from '../router'
import { Message } from 'element-ui'

const API_ROOT = "http://172.17.8.12"

function getMethod(path, data = {}) {
  return axios({
    method: 'get',
    url: API_ROOT + path,
    params: data
  })
}

// post请求(异步请求)
function postMethod(path, data = {}) {
  return axios({
    method: 'post',
    url: API_ROOT + path,
    data: qs.stringify(data)
  })
}

/* 拦截接口订单部分加token */
axios.interceptors.request.use((config) => {
  if (config.url.indexOf('morder') !== -1) {
    if (config.method === 'post') {
      config.data = `${config.data}&access_token=${sessionStorage.getItem('token')}}`
    }
    if (config.method === 'get') {
      Object.assign(config.params, { access_token: sessionStorage.getItem('token') })
    }
  }
  return config;
}, (error) => {
  return Promise.reject(error);
});

/* 拦截接口回应 808返回登陆 */
axios.interceptors.response.use((response) => {
  if (response.data.code === 808) {
    sessionStorage.clear();
    store.dispatch("userLogOut").then(() => {
      router.push({ name: 'login' });
      Message.error('登陆已过期或被登出，请重新登陆!')
    });
  }
  return response
}, (error) => {
  return Promise.reject(error);
});

export default ({
  /* 获取oss图片地址 */
  getOssUrl(data = {}) {
    return getMethod('/front/front/oss-url', data)
  },

  /* 获取oss上传目录 */
  getOssDocAddress() {
    return getMethod('/front/front/generate-file-name')
  },

  /* 获取手机验证码 */
  getPhoneCode(data = {}) {
    return getMethod('/front/front/check-get-code', data)
  },

  /* 密码登陆 */
  passwordLogin(data = {}) {
    return getMethod('/front/front/login', data)
  },

  /* 验证码登陆 */
  codeLogin(data = {}) {
    return getMethod('/front/front/code-login', data)
  },

  /* 注册会员 */
  registerMember(data = {}) {
    return getMethod('/front/front/register', data)
  },

  /* 全局搜索 */
  searchPublicData(data = {}) {
    return getMethod('/front/front/index-search', data)
  },

  /* 主页接口 */
  getIndexData(data = {}) {
    return getMethod('/front/front/index', data)
  },

  /* 合作品牌列表 */
  getBrandList(data = {}) {
    return getMethod('/front/front/brand', data)
  },

  /* 新闻动态列表 */
  getNewsList(data = {}) {
    return getMethod('/front/front/article', data)
  },

  /* 新闻动态详情 */
  getNewsDetail(data = {}) {
    return getMethod('/front/front/article-d', data)
  },

  /* 产品列表数据 */
  getProductList(data = {}) {
    return getMethod('/front/front/product', data)
  },

  /* 产品详情 */
  getProductDetail(data = {}) {
    return getMethod('/front/front/product-d', data)
  },

  /* 案例列表数据 */
  getSolutionList(data = {}) {
    return getMethod('/front/front/case', data)
  },

  /* 案例列表数据 */
  getSolutionDetail(data = {}) {
    return getMethod('/front/front/case-d', data)
  },

  /* 主页立即询价 */
  indexInquiry(data = {}) {
    return getMethod('/front/front/simple-enquiry', data)
  },

  /* 详细询价 */
  detailedInquiry(data = {}) {
    return postMethod('/order/waste-enquiry/front-create', data)
  },

  /* 用户积分 */
  userIntegralList(data = {}) {
    return getMethod('/front/morder/point', data)
  },

  /* 询价单列表 */
  inquiryList(data = {}) {
    return getMethod('/front/morder/enquiry', data)
  },

  /* 询价单取消 */
  inquiryCancel(data = {}) {
    return getMethod('/front/morder/cancel-enquiry', data)
  },

  /* 确认报价 */
  confirmQuote(data = {}) {
    return getMethod('/front/morder/confirm-quotation', data)
  },

  /* 未报价询价单信息 */
  quotingInquiryDetail(data = {}) {
    return getMethod('/front/morder/enquiry-d', data)
  },

  /* 已报价询价单信息 */
  quotedInquiryDetail(data = {}) {
    return getMethod('/front/morder/enquiry-quotation', data)
  },

  /* 核对询价单信息 */
  checkInquiry(data = {}) {
    return getMethod('/front/morder/check-quotation', data)
  },

  /* 销售订单列表 */
  saleOrdersList(data = {}) {
    return getMethod('/front/morder/sale-order', data)
  },

  /* 销售订单详情 */
  saleOrdersDetail(data = {}) {
    return getMethod('/front/morder/sale-order-d', data)
  },

  /* 会员信息 */
  memberInfo(data = {}) {
    return getMethod('/front/morder/user', data)
  },

  /* 新增地址 */
  addAddress(data = {}) {
    return getMethod('/front/morder/address', data)
  },

  /* 删除地址 */
  delAddress(data = {}) {
    return getMethod('/front/morder/address-delete', data)
  },

  /* 新增发票 */
  addInvoice(data = {}) {
    return getMethod('/front/morder/invoice', data)
  },

  /* 删除发票 */
  delInvoice(data = {}) {
    return getMethod('/front/morder/invoice-delete', data)
  },

  /* 会员联系人信息编辑 */
  editMemberInfo(data = {}) {
    return getMethod('/front/morder/msg-edit', data)
  },

  /* 获取会员联系人信息 */
  getMemberInfo(data = {}) {
    return getMethod('/front/morder/get-user-msg', data)
  },

  /* 修改会员密码 */
  resetPassword(data = {}) {
    return getMethod('/front/morder/reset-pwd', data)
  },

  /* 收货地址设置默认 */
  setDefaultAdr(data = {}) {
    return getMethod('/front/morder/address-top', data)
  },

  /* 发票设置默认 */
  setDefaultInv(data = {}) {
    return getMethod('/front/morder/invoice-top', data)
  },

  /* 手机号验证是否注册 */
  forgotPassword(data = {}) {
    return postMethod('/front/front/pwd-get-code', data)
  },

  /* 忘记密码 */
  forgotFirst(data = {}) {
    return postMethod('/front/front/verify-code', data)
  },

  /* 修改密码 */
  forgotSecond(data = {}) {
    return postMethod('/front/front/edit-pwd', data)
  }
})