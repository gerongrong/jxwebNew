const getters = {
  userInfo: state => state.user.userInfo,
  cachedViews: state => state.view.cachedViews,
  indexData: state => state.view.indexData,
}
export default getters