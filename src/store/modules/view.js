const view = {
  state: {
    cachedViews: [],
    indexData: []
  },

  mutations: {
    ADD_CACHED_VIEW: (state, view) => {
      if (state.cachedViews.includes(view.name)) return
      if (view.meta.cache) {
        state.cachedViews.push(view.name)
      }
    },
    SET_INDEX_DATA: (state, data) => {
      state.indexData = data
    }
  },

  actions: {
    addCachedView({ commit }, view) {
      commit('ADD_CACHED_VIEW', view)
    },
    setIndexData({ commit }, data) {
      commit('SET_INDEX_DATA', data)
    }
  }
}

export default view