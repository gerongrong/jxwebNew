const user = {
  state: {
    userInfo: null,
  },

  mutations: {
    SET_USERINFO(state, data) {
      state.userInfo = data
    }

  },

  actions: {
    // 用户名登录
    userLogin({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        commit('SET_USERINFO', userInfo)
        resolve()
      })
    },

    // 用户名登出
    userLogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        commit('SET_USERINFO', null)
        resolve()
      })
    },
  }
}

export default user