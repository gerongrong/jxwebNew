/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/view/layout'
import centerLayout from '@/view/userCenter'

Vue.use(Router)

//静态路由
const constantRouterMap = [{
  path: '/login',
  name: 'login',
  component: () =>
    import ('@/view/login'),
  meta: { title: '登录页' }
}, {
  path: '/register',
  name: 'register',
  component: () =>
    import ('@/view/login/register'),
  meta: { title: '注册页' }
}, {
  path: '/forgotPassword',
  name: 'forgotPassword',
  component: () =>
    import ('@/view/login/forgotPassword'),
  meta: { title: '忘记密码' }
}, {
  path: '/registerSucc',
  name: 'registerSucc',
  component: () =>
    import ('@/view/login/registerSucc'),
  meta: { title: '注册成功' },
}, {
  path: '/',
  component: Layout,
  redirect: { name: 'homePage' },
  children: [{
      path: '/homePage',
      name: 'homePage',
      component: () =>
        import ('@/view/homePage/index'),
      meta: { title: '首页' }
    },
    {
      path: 'search',
      name: 'searchData',
      component: () =>
        import ('@/view/homePage/searchData'),
      meta: { title: '搜索页' }
    }
  ]
}]

//主干路由
const mainRouterMap = [{
    path: '/chainServe',
    component: Layout,
    redirect: { name: 'chainServe' },
    children: [{
      path: '',
      name: 'chainServe',
      component: () =>
        import ('@/view/chainServe/index'),
      meta: { title: '供应链服务', cache: true }
    }]
  },
  {
    path: '/pusInquiry',
    component: Layout,
    redirect: { name: 'pusInquiry' },
    children: [{
      path: '',
      name: 'pusInquiry',
      component: () =>
        import ('@/view/pusInquiry/index'),
      meta: { title: '采购询价' }
    }]
  },
  {
    path: '/ourServe',
    component: Layout,
    redirect: { name: 'ourServe' },
    children: [{
      path: '',
      name: 'ourServe',
      component: () =>
        import ('@/view/ourServe/index'),
      meta: { title: '我们的服务', cache: true }
    }]
  },
  {
    path: '/solution',
    component: Layout,
    redirect: { name: 'solution' },
    children: [{
      path: '',
      name: 'solution',
      component: () =>
        import ('@/view/solution/index'),
      redirect: { name: 'solutionList' },
      meta: { title: '解决方案' },
      children: [{
          path: '/solution',
          name: 'solutionList',
          component: () =>
            import ('@/view/solution/list.vue')
        },
        {
          path: 'detail',
          name: 'solutionDet',
          component: () =>
            import ('@/view/solution/detail.vue'),
          meta: { title: '方案详情' }
        }
      ]
    }]
  },
  {
    path: '/production',
    component: Layout,
    redirect: { name: 'production' },
    children: [{
      path: '',
      name: 'production',
      component: () =>
        import ('@/view/production'),
      redirect: { name: 'productList' },
      meta: { title: '产品列表' },
      children: [{
        path: '/production',
        name: 'productList',
        component: () =>
          import ('@/view/production/list.vue')
      }, {
        path: 'detail',
        name: 'productDetail',
        component: () =>
          import ('@/view/production/detail.vue'),
        meta: { title: '产品详情' }
      }]
    }]
  },
  {
    path: '/aboutUs',
    component: Layout,
    redirect: { name: 'companyInfo' },
    children: [{
        path: 'companyInfo',
        name: 'companyInfo',
        component: () =>
          import ('@/view/aboutUs/companyInfo/index'),
        meta: { title: '公司简介', cache: true }
      }, {
        path: 'serveOnline',
        name: 'serveOnline',
        component: () =>
          import ('@/view/aboutUs/serveOnline/index'),
        meta: { title: '在线服务' }
      },
      {
        path: 'contactUs',
        name: 'contactUs',
        component: () =>
          import ('@/view/aboutUs/contactUs/index'),
        meta: { title: '联系我们', cache: true }
      },
      {
        path: 'news',
        name: 'news',
        component: () =>
          import ('@/view/aboutUs/news'),
        redirect: { name: 'newsList' },
        meta: { title: '新闻列表' },
        children: [{
          path: '',
          name: 'newsList',
          component: () =>
            import ('@/view/aboutUs/news/list.vue'),
        }, {
          path: 'detail',
          name: 'newsDetail',
          component: () =>
            import ('@/view/aboutUs/news/detail.vue'),
          meta: { title: '新闻详情' }
        }]

      },
      {
        path: 'brandList',
        name: 'brandList',
        component: () =>
          import ('@/view/aboutUs/brandList/index'),
        meta: { title: '合作品牌', cache: true }
      }
    ]
  },
]

//用户中心路由
const cencerRouterMap = [{
  path: '/userCenter',
  component: Layout,
  redirect: { name: 'memberCenter' },
  children: [{
      path: 'memberCenter',
      component: centerLayout,
      redirect: { name: 'memberCenter' },
      children: [{
        path: '/userCenter/memberCenter',
        name: 'memberCenter',
        component: () =>
          import ('@/view/userCenter/memberCenter'),
        meta: { title: '会员中心' }
      }]
    },
    {
      path: 'accountConfig',
      component: centerLayout,
      redirect: { name: 'accountConfig' },
      children: [{
        path: '/userCenter/accountConfig',
        name: 'accountConfig',
        component: () =>
          import ('@/view/userCenter/accountConfig'),
        meta: { title: '账户设置' }
      }]
    },
    {
      path: 'feedback',
      component: centerLayout,
      redirect: { name: 'feedback' },
      children: [{
        path: '/userCenter/feedback',
        name: 'feedback',
        component: () =>
          import ('@/view/userCenter/feedback'),
        meta: { title: '意见反馈' }
      }]
    },
    {
      path: 'assetCenter',
      component: centerLayout,
      redirect: { name: 'integral' },
      children: [{
        path: 'integral',
        name: 'integral',
        component: () =>
          import ('@/view/userCenter/assetCenter/integral'),
        meta: { title: '我的积分' }
      }, {
        path: 'coupon',
        name: 'coupon',
        component: () =>
          import ('@/view/userCenter/assetCenter/coupon'),
        meta: { title: '我的优惠券' }
      }]
    },
    {
      path: 'returnedGoods',
      component: centerLayout,
      redirect: { name: 'returning' },
      children: [{
          path: 'returning',
          name: 'returning',
          component: () =>
            import ('@/view/userCenter/returnedGoods/returning'),
          redirect: { name: 'returningL' },
          meta: { title: '退换中' },
          children: [{
            path: '',
            name: 'returningL',
            component: () =>
              import ('@/view/userCenter/returnedGoods/returning/list.vue')
          }, {
            path: 'detail',
            name: 'returningD',
            component: () =>
              import ('@/view/userCenter/returnedGoods/returning/detail.vue'),
            meta: { title: '退换货单详情页(退换中)' }
          }]
        },
        {
          path: 'returned',
          name: 'returned',
          component: () =>
            import ('@/view/userCenter/returnedGoods/returned'),
          redirect: { name: 'returnedL' },
          meta: { title: '已完成' },
          children: [{
            path: '',
            name: 'returnedL',
            component: () =>
              import ('@/view/userCenter/returnedGoods/returned/list.vue')
          }, {
            path: 'detail',
            name: 'returnedD',
            component: () =>
              import ('@/view/userCenter/returnedGoods/returned/detail.vue'),
            meta: { title: '退换货单详情页(已退换)' }
          }]
        }
      ]
    },
    {
      path: 'myOrder',
      component: centerLayout,
      redirect: { name: 'saleOrders' },
      children: [{
          path: 'saleOrders',
          name: 'saleOrders',
          component: () =>
            import ('@/view/userCenter/myOrder/saleOrders'),
          redirect: { name: 'saleOrdersL' },
          meta: { title: '销售订单' },
          children: [{
            path: '/userCenter/myOrder/saleOrders',
            name: 'saleOrdersL',
            component: () =>
              import ('@/view/userCenter/myOrder/saleOrders/list.vue')
          }, {
            path: 'detail',
            name: 'saleOrdersD',
            component: () =>
              import ('@/view/userCenter/myOrder/saleOrders/detail.vue'),
            meta: { title: '订单详情' }
          }]
        }, {
          path: 'cancelOrders',
          name: 'cancelOrders',
          component: () =>
            import ('@/view/userCenter/myOrder/cancelOrders'),
          redirect: { name: 'cancelOrdersL' },
          meta: { title: '已取消订单' },
          children: [{
            path: '/userCenter/myOrder/cancelOrders',
            name: 'cancelOrdersL',
            component: () =>
              import ('@/view/userCenter/myOrder/cancelOrders/list.vue')
          }, {
            path: 'detail',
            name: 'cancelOrdersD',
            component: () =>
              import ('@/view/userCenter/myOrder/cancelOrders/detail.vue'),
            meta: { title: '订单详情' }
          }]
        }, {
          path: 'inquiryOrders',
          name: 'inquiryOrders',
          component: () =>
            import ('@/view/userCenter/myOrder/inquiryOrders'),
          redirect: { name: 'inquiryOrdersL' },
          meta: { title: '询价订单' },
          children: [{
              path: '/userCenter/myOrder/inquiryOrders',
              name: 'inquiryOrdersL',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/list.vue')
            }, {
              path: 'detail',
              name: 'inquiryOrdersLD',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/detail.vue'),
              meta: { title: '订单详情' }
            },
            {
              path: 'viewPlan',
              name: 'inquiryViewplan',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/viewPlan.vue'),
              meta: { title: '报价方案' }
            },
            {
              path: 'checkInfo',
              name: 'inquirycheckInfo',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/checkInfo.vue'),
              meta: { title: '核对订单信息' }
            },
            {
              path: 'cancelInfo',
              name: 'inquirycancelInfo',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/cancelInfo.vue'),
              meta: { title: '已取消' }
            },
            {
              path: 'confirmed',
              name: 'inquiryconfirmed',
              component: () =>
                import ('@/view/userCenter/myOrder/inquiryOrders/confirmed.vue'),
              meta: { title: '已确认' }
            },
          ]
        },
        {
          path: 'invOrders',
          name: 'invOrders',
          component: () =>
            import ('@/view/userCenter/myOrder/invOrders'),
          redirect: { name: 'invOrdersL' },
          meta: { title: '补开发票' },
          children: [{
              path: '/userCenter/myOrder/invOrders',
              name: 'invOrdersL',
              component: () =>
                import ('@/view/userCenter/myOrder/invOrders/list.vue')
            }, {
              path: 'detail',
              name: 'invOrdersD',
              component: () =>
                import ('@/view/userCenter/myOrder/invOrders/detail.vue'),
              meta: { title: '发票详情' }
            },
            {
              path: 'applyInv',
              name: 'invOrdersA',
              component: () =>
                import ('@/view/userCenter/myOrder/invOrders/applyInv.vue'),
              meta: { title: '申请发票' }
            }
          ]
        }
      ]
    }
  ]
}]

export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRouterMap, ...mainRouterMap, ...cencerRouterMap]
})