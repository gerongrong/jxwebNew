/* eslint-disable */
let addressArr = [{
    "region_id": 1,
    "label": "北京",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
      "region_id": 2,
      "label": "北京市",
      "p_region_id": 1,
      "region_grade": 2
    }]
  },
  {
    "region_id": 21,
    "label": "上海",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
      "region_id": 22,
      "label": "上海市",
      "p_region_id": 21,
      "region_grade": 2
    }]
  },
  {
    "region_id": 42,
    "label": "天津",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
      "region_id": 43,
      "label": "天津市",
      "p_region_id": 42,
      "region_grade": 2
    }]
  },
  {
    "region_id": 62,
    "label": "重庆",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
      "region_id": 63,
      "label": "重庆市",
      "p_region_id": 62,
      "region_grade": 2
    }]
  },
  {
    "region_id": 104,
    "label": "安徽",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 105,
        "label": "合肥市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 113,
        "label": "安庆市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 125,
        "label": "蚌埠市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 133,
        "label": "亳州市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 138,
        "label": "巢湖市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 144,
        "label": "池州市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 149,
        "label": "滁州市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 158,
        "label": "阜阳市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 167,
        "label": "淮北市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 172,
        "label": "淮南市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 179,
        "label": "黄山市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 187,
        "label": "六安市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 195,
        "label": "马鞍山市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 200,
        "label": "宿州市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 206,
        "label": "铜陵市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 211,
        "label": "芜湖市",
        "p_region_id": 104,
        "region_grade": 2
      },
      {
        "region_id": 219,
        "label": "宣城市",
        "p_region_id": 104,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 227,
    "label": "福建",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 228,
        "label": "福州市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 242,
        "label": "龙岩市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 250,
        "label": "南平市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 261,
        "label": "宁德市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 271,
        "label": "莆田市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 277,
        "label": "泉州市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 290,
        "label": "三明市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 303,
        "label": "厦门市",
        "p_region_id": 227,
        "region_grade": 2
      },
      {
        "region_id": 310,
        "label": "漳州市",
        "p_region_id": 227,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 322,
    "label": "甘肃",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 323,
        "label": "兰州市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 332,
        "label": "白银市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 338,
        "label": "定西市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 346,
        "label": "甘南藏族自治州",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 355,
        "label": "嘉峪关市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 356,
        "label": "金昌市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 359,
        "label": "酒泉市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 367,
        "label": "临夏回族自治州",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 376,
        "label": "陇南市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 386,
        "label": "平凉市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 394,
        "label": "庆阳市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 403,
        "label": "天水市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 411,
        "label": "武威市",
        "p_region_id": 322,
        "region_grade": 2
      },
      {
        "region_id": 416,
        "label": "张掖市",
        "p_region_id": 322,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 423,
    "label": "广东",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 424,
        "label": "广州市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 437,
        "label": "潮州市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 441,
        "label": "东莞市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 442,
        "label": "佛山市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 448,
        "label": "河源市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 455,
        "label": "惠州市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 461,
        "label": "江门市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 469,
        "label": "揭阳市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 475,
        "label": "茂名市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 482,
        "label": "梅江区",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 483,
        "label": "梅州市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 491,
        "label": "清远市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 500,
        "label": "汕头市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 508,
        "label": "汕尾市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 513,
        "label": "韶关市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 524,
        "label": "深圳市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 531,
        "label": "阳江市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 536,
        "label": "云浮市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 542,
        "label": "湛江市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 552,
        "label": "肇庆市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 561,
        "label": "中山市",
        "p_region_id": 423,
        "region_grade": 2
      },
      {
        "region_id": 562,
        "label": "珠海市",
        "p_region_id": 423,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 566,
    "label": "广西",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 567,
        "label": "南宁市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 580,
        "label": "百色市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 593,
        "label": "北海市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 598,
        "label": "崇左市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 606,
        "label": "防城港市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 611,
        "label": "贵港市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 617,
        "label": "桂林市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 635,
        "label": "河池市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 647,
        "label": "贺州市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 652,
        "label": "来宾市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 659,
        "label": "柳州市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 670,
        "label": "钦州市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 675,
        "label": "梧州市",
        "p_region_id": 566,
        "region_grade": 2
      },
      {
        "region_id": 683,
        "label": "玉林市",
        "p_region_id": 566,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 690,
    "label": "贵州",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 691,
        "label": "贵阳市",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 702,
        "label": "安顺市",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 709,
        "label": "毕节地区",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 718,
        "label": "六盘水市",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 723,
        "label": "黔东南苗族侗族自治州",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 740,
        "label": "黔南布依族苗族自治州",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 753,
        "label": "黔西南布依族苗族自治州",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 762,
        "label": "铜仁地区",
        "p_region_id": 690,
        "region_grade": 2
      },
      {
        "region_id": 773,
        "label": "遵义市",
        "p_region_id": 690,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 788,
    "label": "海南",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 789,
        "label": "海口市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 794,
        "label": "白沙黎族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 795,
        "label": "保亭黎族苗族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 796,
        "label": "昌江黎族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 797,
        "label": "澄迈县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 798,
        "label": "儋州市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 799,
        "label": "定安县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 800,
        "label": "东方市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 801,
        "label": "乐东黎族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 802,
        "label": "临高县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 803,
        "label": "陵水黎族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 804,
        "label": "南沙群岛",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 805,
        "label": "琼海市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 806,
        "label": "琼中黎族苗族自治县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 807,
        "label": "三亚市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 808,
        "label": "屯昌县",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 809,
        "label": "万宁市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 810,
        "label": "文昌市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 811,
        "label": "五指山市",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 812,
        "label": "西沙群岛",
        "p_region_id": 788,
        "region_grade": 2
      },
      {
        "region_id": 813,
        "label": "中沙群岛的岛礁及其海域",
        "p_region_id": 788,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 814,
    "label": "河北",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 815,
        "label": "石家庄市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 839,
        "label": "保定市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 865,
        "label": "沧州市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 882,
        "label": "承德市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 894,
        "label": "邯郸市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 914,
        "label": "衡水市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 926,
        "label": "廊坊市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 937,
        "label": "秦皇岛市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 945,
        "label": "唐山市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 960,
        "label": "邢台市",
        "p_region_id": 814,
        "region_grade": 2
      },
      {
        "region_id": 980,
        "label": "张家口市",
        "p_region_id": 814,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 998,
    "label": "河南",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 999,
        "label": "郑州市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1012,
        "label": "安阳市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1022,
        "label": "鹤壁市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1028,
        "label": "济源市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1029,
        "label": "焦作市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1041,
        "label": "开封市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1052,
        "label": "洛阳市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1068,
        "label": "漯河市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1074,
        "label": "南阳市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1088,
        "label": "平顶山市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1099,
        "label": "濮阳市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1106,
        "label": "三门峡市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1113,
        "label": "商丘市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1123,
        "label": "新乡市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1136,
        "label": "信阳市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1147,
        "label": "许昌市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1154,
        "label": "周口市",
        "p_region_id": 998,
        "region_grade": 2
      },
      {
        "region_id": 1165,
        "label": "驻马店市",
        "p_region_id": 998,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1176,
    "label": "黑龙江",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1177,
        "label": "哈尔滨市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1197,
        "label": "大庆市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1207,
        "label": "大兴安岭地区",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1211,
        "label": "鹤岗市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1220,
        "label": "黑河市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1227,
        "label": "鸡西市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1237,
        "label": "佳木斯市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1249,
        "label": "牡丹江市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1260,
        "label": "七台河市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1265,
        "label": "齐齐哈尔市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1282,
        "label": "双鸭山市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1291,
        "label": "绥化市",
        "p_region_id": 1176,
        "region_grade": 2
      },
      {
        "region_id": 1302,
        "label": "伊春市",
        "p_region_id": 1176,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1320,
    "label": "湖北",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1321,
        "label": "武汉市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1335,
        "label": "鄂州市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1339,
        "label": "恩施土家族苗族自治州",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1348,
        "label": "黄冈市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1359,
        "label": "黄石市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1366,
        "label": "荆门市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1372,
        "label": "荆州市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1381,
        "label": "潜江市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1382,
        "label": "神农架林区",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1383,
        "label": "十堰市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1392,
        "label": "随州市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1395,
        "label": "天门市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1396,
        "label": "仙桃市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1397,
        "label": "咸宁市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1404,
        "label": "襄樊市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1414,
        "label": "孝感市",
        "p_region_id": 1320,
        "region_grade": 2
      },
      {
        "region_id": 1422,
        "label": "宜昌市",
        "p_region_id": 1320,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1436,
    "label": "湖南",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1437,
        "label": "长沙市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1447,
        "label": "常德市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1457,
        "label": "郴州市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1469,
        "label": "衡阳市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1482,
        "label": "怀化市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1495,
        "label": "娄底市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1501,
        "label": "邵阳市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1514,
        "label": "湘潭市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1520,
        "label": "湘西土家族苗族自治州",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1529,
        "label": "益阳市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1536,
        "label": "永州市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1548,
        "label": "岳阳市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1558,
        "label": "张家界市",
        "p_region_id": 1436,
        "region_grade": 2
      },
      {
        "region_id": 1563,
        "label": "株洲市",
        "p_region_id": 1436,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1573,
    "label": "吉林",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1574,
        "label": "长春市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1585,
        "label": "白城市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1591,
        "label": "白山市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1598,
        "label": "吉林市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1608,
        "label": "辽源市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1613,
        "label": "四平市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1620,
        "label": "松原市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1626,
        "label": "通化市",
        "p_region_id": 1573,
        "region_grade": 2
      },
      {
        "region_id": 1634,
        "label": "延边朝鲜族自治州",
        "p_region_id": 1573,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1643,
    "label": "江苏",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1644,
        "label": "南京市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1658,
        "label": "常州市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1666,
        "label": "淮安市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1675,
        "label": "连云港市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1683,
        "label": "南通市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1692,
        "label": "苏州市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1704,
        "label": "宿迁市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1710,
        "label": "泰州市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1717,
        "label": "无锡市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1726,
        "label": "徐州市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1738,
        "label": "盐城市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1748,
        "label": "扬州市",
        "p_region_id": 1643,
        "region_grade": 2
      },
      {
        "region_id": 1756,
        "label": "镇江市",
        "p_region_id": 1643,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1763,
    "label": "江西",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1764,
        "label": "南昌市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1774,
        "label": "抚州市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1786,
        "label": "赣州市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1805,
        "label": "吉安市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1819,
        "label": "景德镇市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1824,
        "label": "九江市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1837,
        "label": "萍乡市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1843,
        "label": "上饶市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1856,
        "label": "新余市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1859,
        "label": "宜春市",
        "p_region_id": 1763,
        "region_grade": 2
      },
      {
        "region_id": 1870,
        "label": "鹰潭市",
        "p_region_id": 1763,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1874,
    "label": "辽宁",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1875,
        "label": "沈阳市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1889,
        "label": "鞍山市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1897,
        "label": "本溪市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1904,
        "label": "朝阳市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1912,
        "label": "大连市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1923,
        "label": "丹东市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1930,
        "label": "抚顺市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1938,
        "label": "阜新市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1946,
        "label": "葫芦岛市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1953,
        "label": "锦州市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1961,
        "label": "辽阳市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1969,
        "label": "盘锦市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1974,
        "label": "铁岭市",
        "p_region_id": 1874,
        "region_grade": 2
      },
      {
        "region_id": 1982,
        "label": "营口市",
        "p_region_id": 1874,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 1989,
    "label": "内蒙古",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 1990,
        "label": "呼和浩特市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2000,
        "label": "阿拉善盟",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2004,
        "label": "巴彦淖尔市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2012,
        "label": "包头市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2022,
        "label": "赤峰市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2035,
        "label": "鄂尔多斯市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2044,
        "label": "呼伦贝尔市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2058,
        "label": "通辽市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2067,
        "label": "乌海市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2071,
        "label": "乌兰察布市",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2083,
        "label": "锡林郭勒盟",
        "p_region_id": 1989,
        "region_grade": 2
      },
      {
        "region_id": 2096,
        "label": "兴安盟",
        "p_region_id": 1989,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2103,
    "label": "宁夏",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2104,
        "label": "银川市",
        "p_region_id": 2103,
        "region_grade": 2
      },
      {
        "region_id": 2111,
        "label": "固原市",
        "p_region_id": 2103,
        "region_grade": 2
      },
      {
        "region_id": 2117,
        "label": "石嘴山市",
        "p_region_id": 2103,
        "region_grade": 2
      },
      {
        "region_id": 2121,
        "label": "吴忠市",
        "p_region_id": 2103,
        "region_grade": 2
      },
      {
        "region_id": 2126,
        "label": "中卫市",
        "p_region_id": 2103,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2130,
    "label": "青海",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2131,
        "label": "西宁市",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2139,
        "label": "果洛藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2146,
        "label": "海北藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2151,
        "label": "海东地区",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2158,
        "label": "海南藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2164,
        "label": "海西蒙古族藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2170,
        "label": "黄南藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      },
      {
        "region_id": 2175,
        "label": "玉树藏族自治州",
        "p_region_id": 2130,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2182,
    "label": "山东",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2183,
        "label": "济南市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2194,
        "label": "滨州市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2202,
        "label": "德州市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2214,
        "label": "东营市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2220,
        "label": "菏泽市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2230,
        "label": "济宁市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2243,
        "label": "莱芜市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2246,
        "label": "聊城市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2255,
        "label": "临沂市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2268,
        "label": "青岛市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2281,
        "label": "日照市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2286,
        "label": "泰安市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2293,
        "label": "威海市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2298,
        "label": "潍坊市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2311,
        "label": "烟台市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2324,
        "label": "枣庄市",
        "p_region_id": 2182,
        "region_grade": 2
      },
      {
        "region_id": 2331,
        "label": "淄博市",
        "p_region_id": 2182,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2340,
    "label": "山西",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2341,
        "label": "太原市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2352,
        "label": "长治市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2366,
        "label": "大同市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2378,
        "label": "晋城市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2385,
        "label": "晋中市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2397,
        "label": "临汾市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2415,
        "label": "吕梁市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2429,
        "label": "朔州市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2436,
        "label": "忻州市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2451,
        "label": "阳泉市",
        "p_region_id": 2340,
        "region_grade": 2
      },
      {
        "region_id": 2457,
        "label": "运城市",
        "p_region_id": 2340,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2471,
    "label": "陕西",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2472,
        "label": "西安市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2486,
        "label": "安康市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2497,
        "label": "宝鸡市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2510,
        "label": "汉中市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2522,
        "label": "商洛市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2530,
        "label": "铜川市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2535,
        "label": "渭南市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2547,
        "label": "咸阳市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2562,
        "label": "延安市",
        "p_region_id": 2471,
        "region_grade": 2
      },
      {
        "region_id": 2576,
        "label": "榆林市",
        "p_region_id": 2471,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2589,
    "label": "四川",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2590,
        "label": "成都市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2610,
        "label": "阿坝藏族羌族自治州",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2624,
        "label": "巴中市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2629,
        "label": "达州市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2637,
        "label": "德阳市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2644,
        "label": "甘孜藏族自治州",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2663,
        "label": "广安市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2669,
        "label": "广元市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2677,
        "label": "乐山市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2689,
        "label": "凉山彝族自治州",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2707,
        "label": "泸州市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2715,
        "label": "眉山市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2722,
        "label": "绵阳市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2732,
        "label": "内江市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2738,
        "label": "南充市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2748,
        "label": "攀枝花市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2754,
        "label": "遂宁市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2760,
        "label": "雅安市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2769,
        "label": "宜宾市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2780,
        "label": "资阳市",
        "p_region_id": 2589,
        "region_grade": 2
      },
      {
        "region_id": 2785,
        "label": "自贡市",
        "p_region_id": 2589,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2792,
    "label": "西藏",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2793,
        "label": "拉萨市",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2802,
        "label": "阿里地区",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2810,
        "label": "昌都地区",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2822,
        "label": "林芝地区",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2830,
        "label": "那曲地区",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2841,
        "label": "日喀则地区",
        "p_region_id": 2792,
        "region_grade": 2
      },
      {
        "region_id": 2860,
        "label": "山南地区",
        "p_region_id": 2792,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2873,
    "label": "新疆",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2874,
        "label": "乌鲁木齐市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2883,
        "label": "阿克苏地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2893,
        "label": "阿拉尔市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2894,
        "label": "阿勒泰地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2902,
        "label": "巴音郭楞蒙古自治州",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2912,
        "label": "博尔塔拉蒙古自治州",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2916,
        "label": "昌吉回族自治州",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2925,
        "label": "哈密地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2929,
        "label": "和田地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2938,
        "label": "喀什地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2951,
        "label": "克拉玛依市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2956,
        "label": "克孜勒苏柯尔克孜自治州",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2961,
        "label": "石河子市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2962,
        "label": "塔城地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2970,
        "label": "图木舒克市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2971,
        "label": "吐鲁番地区",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2975,
        "label": "五家渠市",
        "p_region_id": 2873,
        "region_grade": 2
      },
      {
        "region_id": 2976,
        "label": "伊犁哈萨克自治州",
        "p_region_id": 2873,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 2987,
    "label": "云南",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 2988,
        "label": "昆明市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3003,
        "label": "保山市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3009,
        "label": "楚雄彝族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3020,
        "label": "大理白族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3033,
        "label": "德宏傣族景颇族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3039,
        "label": "迪庆藏族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3043,
        "label": "红河哈尼族彝族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3057,
        "label": "丽江市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3063,
        "label": "临沧市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3072,
        "label": "怒江傈僳族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3077,
        "label": "曲靖市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3087,
        "label": "思茅市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3098,
        "label": "文山壮族苗族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3107,
        "label": "西双版纳傣族自治州",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3111,
        "label": "玉溪市",
        "p_region_id": 2987,
        "region_grade": 2
      },
      {
        "region_id": 3121,
        "label": "昭通市",
        "p_region_id": 2987,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 3133,
    "label": "浙江",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 3134,
        "label": "杭州市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3148,
        "label": "湖州市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3154,
        "label": "嘉兴市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3162,
        "label": "金华市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3172,
        "label": "丽水市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3182,
        "label": "宁波市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3194,
        "label": "衢州市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3201,
        "label": "绍兴市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3208,
        "label": "台州市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3218,
        "label": "温州市",
        "p_region_id": 3133,
        "region_grade": 2
      },
      {
        "region_id": 3230,
        "label": "舟山市",
        "p_region_id": 3133,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 3235,
    "label": "香港",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 3236,
        "label": "九龙",
        "p_region_id": 3235,
        "region_grade": 2
      },
      {
        "region_id": 3237,
        "label": "香港岛",
        "p_region_id": 3235,
        "region_grade": 2
      },
      {
        "region_id": 3238,
        "label": "新界",
        "p_region_id": 3235,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 3239,
    "label": "澳门",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 3240,
        "label": "澳门半岛",
        "p_region_id": 3239,
        "region_grade": 2
      },
      {
        "region_id": 3241,
        "label": "离岛",
        "p_region_id": 3239,
        "region_grade": 2
      }
    ]
  },
  {
    "region_id": 3242,
    "label": "台湾",
    "p_region_id": 0,
    "region_grade": 1,
    "cities": [{
        "region_id": 3243,
        "label": "台北市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3244,
        "label": "高雄市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3245,
        "label": "高雄县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3246,
        "label": "花莲县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3247,
        "label": "基隆市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3248,
        "label": "嘉义市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3249,
        "label": "嘉义县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3250,
        "label": "金门县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3251,
        "label": "苗栗县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3252,
        "label": "南投县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3253,
        "label": "澎湖县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3254,
        "label": "屏东县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3255,
        "label": "台北县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3256,
        "label": "台东县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3257,
        "label": "台南市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3258,
        "label": "台南县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3259,
        "label": "台中市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3260,
        "label": "台中县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3261,
        "label": "桃园县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3262,
        "label": "新竹市",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3263,
        "label": "新竹县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3264,
        "label": "宜兰县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3265,
        "label": "云林县",
        "p_region_id": 3242,
        "region_grade": 2
      },
      {
        "region_id": 3266,
        "label": "彰化县",
        "p_region_id": 3242,
        "region_grade": 2
      }
    ]
  }
]


export default addressArr