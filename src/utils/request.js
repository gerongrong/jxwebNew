import axios from 'axios'

const Service = axios.create({
  baseURL: process.env.BASE_API,
  timeout: 5000
})
export default Service